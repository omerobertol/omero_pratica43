package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Omero Francisco Bertol
 */

public class TrianguloEquilatero extends Retangulo {
   
  public TrianguloEquilatero(double lado) {
    super(lado, lado * Math.sqrt(3) / 2);
  }
    
  @Override
  public double getArea() {
    return super.getArea() / 2;
  }

  @Override
  public double getPerimetro() {
    return 3 * getBase();
  }

  public double getLado() {
    return getBase();
  }

  @Override
  public String toString() {
    return getNome() + " [" + getLado() + "]";
  }
    
}