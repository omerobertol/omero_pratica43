package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Omero Francisco Bertol
 */

public class Quadrado extends Retangulo {
    
  public Quadrado(double lado) {
    super(lado, lado);
  }
  
  public double getLado() {
    return getBase();
  }

  @Override
  public String toString() {
    return getNome() + " [" + getLado() + "]";
  }
    
}
