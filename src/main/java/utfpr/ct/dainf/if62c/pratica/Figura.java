package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Omero Francisco Bertol
 */

public interface Figura {
  public String getNome();
  public double getPerimetro();
  public double getArea();
}