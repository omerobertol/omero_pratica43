package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Omero Francisco Bertol
 */

public interface FiguraComLados extends Figura {
  public double getLadoMenor();
  public double getLadoMaior();
}