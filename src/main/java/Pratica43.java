import utfpr.ct.dainf.if62c.pratica.Quadrado;
import utfpr.ct.dainf.if62c.pratica.Retangulo;
import utfpr.ct.dainf.if62c.pratica.TrianguloEquilatero;

/**
 *
 * @author Omero Francisco Bertol
 */

public class Pratica43 {

  public static void main(String[] args) {
    Retangulo retangulo = new Retangulo(3.0, 4.0);
    Quadrado quadrado = new Quadrado(1.0);
    TrianguloEquilatero trianguloEquilatero = new TrianguloEquilatero(2.0);
            
    System.out.println("Area de " + retangulo + " = " + retangulo.getArea());
    System.out.println("Perímetro de " + retangulo + " = " + retangulo.getPerimetro());
    
    System.out.println();
    
    System.out.println("Area de " + quadrado + " = " + quadrado.getArea());
    System.out.println("Perímetro de " + quadrado + " = " + quadrado.getPerimetro());  
    
    System.out.println();
    
    System.out.println("Area de " + trianguloEquilatero + " = " + trianguloEquilatero.getArea());
    System.out.println("Perímetro de " + trianguloEquilatero + " = " + trianguloEquilatero.getPerimetro());     
  }
    
}